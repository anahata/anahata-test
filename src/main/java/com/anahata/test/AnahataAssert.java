package com.anahata.test;

import java.util.Collection;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.Validate;
import static org.junit.Assert.*;

/**
 * Additional JUnit asserts.
 * 
 * @author Robert Nagajek
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class AnahataAssert {
    /**
     * Assert that two comparable values are the same.
     * TODO Handle nulls.
     * 
     * @param <T> The type.
     * @param expected The expected value.
     * @param actual The actual value.
     */
    public static <T extends Comparable<T>> void assertComparable(T expected, T actual) {
        assertTrue("Assertion failed, expected <" + expected + "> but received <" + actual + ">", expected.compareTo(actual) == 0);
    }
    
    /**
     * Compare two collections irrespective of ordering.
     *
     * @param expected The expected values. Can be null.
     * @param actual Values to compare. Can be null.
     */
    public static void assertCollectionEquals(Collection expected, Collection actual) {
        if (expected == null && actual == null) {
            return;
        }

        if (expected == null) {
            fail("expected is null, actual is " + actual);
        }

        if (actual == null) {
            fail("actual is null, expected is " + actual);
        }

        StringBuilder msg = new StringBuilder();

        for (Object obj : actual) {
            if (!expected.contains(obj)) {
                if (msg.length() > 0) {
                    msg.append(", ");
                }

                msg.append("expected does not contain actual object ");
                msg.append(obj);
            }
        }

        for (Object obj : expected) {
            if (!actual.contains(obj)) {
                if (msg.length() > 0) {
                    msg.append(", ");
                }

                msg.append("actual does not contain expected object ");
                msg.append(obj);
            }
        }

        if (msg.length() > 0) {
            fail(msg.toString());
        }
    }
    
    /**
     * Check that a collection contains an element.
     * 
     * @param <T> The type of the element to check.
     * @param expected The expected element value. Required.
     * @param collection The collection to check against. Required.
     * @throws NullPointerException If expected or collection are null.
     */
    public static <T> void assertCollectionContains(T expected, Collection<T> collection) {
        Validate.notNull(expected);
        Validate.notNull(collection);
        
        if (!collection.contains(expected)) {
            fail("The given collection does not contain expected with value " + expected);
        }
    }
    
    /**
     * Check that a collection does not contain an element.
     * 
     * @param <T> The type of the element to check.
     * @param expected The expected element value. Required.
     * @param collection The collection to check against. Required.
     * @throws NullPointerException If expected or collection are null.
     */
    public static <T> void assertCollectionDoesNotContain(T expected, Collection<T> collection) {
        Validate.notNull(expected);
        Validate.notNull(collection);
        
        if (collection.contains(expected)) {
            fail("The given collection contains value " + expected);
        }
    }
}
