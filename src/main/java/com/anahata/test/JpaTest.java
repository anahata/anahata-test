package com.anahata.test;

import com.anahata.util.validation.ValidationUtils;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.Validate;
import org.eclipse.persistence.jpa.JpaEntityManager;
import org.eclipse.persistence.queries.AttributeGroup;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;

/**
 * A base class for running JPA/eclipselink based unit tests. It is assumed the underlying database is HSQLDB, which is
 * cleared out between each test. The persistence unit name is passed in system property 'anahatatest.persistenceunit',
 * and is assumed that it is set up to create tables.
 *
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
@Slf4j
public abstract class JpaTest {
    protected static EntityManagerFactory emFactory;

    protected static EntityManager em;

    protected static EntityTransaction tx;

    @BeforeClass
    public static void setUpClassJpaTest() {
        final String puName = System.getProperty("anahatatest.persistenceunit");
        Validate.validState(puName != null, "System property anahatatest.persistenceunit is not present");
        emFactory = Persistence.createEntityManagerFactory(puName);
        em = emFactory.createEntityManager();
        tx = em.getTransaction();
    }

    @AfterClass
    public static void tearDownClassJpaTest() {
        if (em != null) {
            em.close();
        }
        em = null;
        tx = null;
    }

    @Before
    public void setUpJpaTest() {
        log.debug("setUpJpaTest");

        if (isTruncateSchemaBetweenTests()) {
            log.debug("Deleting all data in schema");
            begin();
            em.createNativeQuery("truncate schema public restart identity and commit no check").executeUpdate();
            em.createNativeQuery("create table if not exists sequence(seq_name varchar(50), seq_count decimal(38))").executeUpdate();
            em.createNativeQuery("insert into sequence(seq_name, seq_count) values ('SEQ_GEN', 0)").executeUpdate();
            commit();
        }
    }

    /**
     * Hook for subclasses to decide whether all data should be deleted in between tests
     *
     * @return
     */
    protected boolean isTruncateSchemaBetweenTests() {
        return true;
    }

    @After
    public void tearDownJpaTest() {
        if (tx.isActive()) {
            commit();
        }

        if (isTruncateSchemaBetweenTests()) {
            begin();
            em.createNativeQuery("truncate schema public restart identity and commit no check").executeUpdate();
            commit();
        }
        em.clear();
    }

    /**
     * Begin a transaction.
     */
    protected void begin() {
        tx.begin();
    }

    /**
     * Commit a transaction.
     */
    protected void commit() {
        try {
            tx.commit();
        } catch (Throwable t) {
            String message = ValidationUtils.getConstraintValidationDetails(t);
            log.error(message, t);
            throw t;
        }
    }

    /**
     * Rollback a transaction.
     */
    protected void rollback() {
        tx.rollback();
    }

    /**
     * Flush entity manager updates to the database.
     */
    protected void flush() {
        try {
            em.flush();
        } catch (Throwable t) {
            String message = ValidationUtils.getConstraintValidationDetails(t);
            log.error(message, t);
            throw t;
        }
    }

    /**
     * Flush and commit.
     */
    protected void flommit() {
        try {
            em.flush();
            tx.commit();
        } catch (Throwable t) {
            String message = ValidationUtils.getConstraintValidationDetails(t);
            log.error(message, t);
            throw t;
        }
    }

    /**
     * Persist a detached entity.
     *
     * @param entity The entity.
     */
    protected void persist(Object entity) {
        try {
            em.persist(entity);
        } catch (Throwable t) {
            String message = ValidationUtils.getConstraintValidationDetails(t);
            log.error(message, t);
            throw t;
        }
    }

    /**
     * Persist and flush an entity.
     *
     * @param entity The entity.
     */
    protected void perflush(Object entity) {
        try {
            em.persist(entity);
            em.flush();
        } catch (Throwable t) {
            String message = ValidationUtils.getConstraintValidationDetails(t);
            log.error(message, t);
            throw t;
        }
    }

    /**
     * Persist, flush and commit.
     *
     * @param entity The entity to persist.
     */
    protected void perflommit(Object entity) {
        try {
            em.persist(entity);
            em.flush();
            tx.commit();
        } catch (Throwable t) {
            String message = ValidationUtils.getConstraintValidationDetails(t);
            log.error(message, t);
            throw t;
        }
    }

    /**
     * Detatch an entity.
     *
     * @param entity The entity.
     */
    protected void detach(Object entity) {
        em.detach(entity);
    }

    /**
     * Execute an update query.
     *
     * @param query The query.
     * @return The number of rows update.
     */
    protected int update(String query) {
        return em.createQuery(query).executeUpdate();
    }

    /**
     * Merge an entity into the entity manager.
     *
     * @param <T>    The entity type.
     * @param entity The entity.
     * @return The merged entity.
     */
    protected <T> T merge(T entity) {
        try {
            return em.merge(entity);
        } catch (Throwable t) {
            String message = ValidationUtils.getConstraintValidationDetails(t);
            log.error(message, t);
            throw t;
        }
    }

    /**
     * Create a copy of an entity using an eclipselink AttributeGroup.
     *
     * @param <T>    The entity type.
     * @param entity The entity.
     * @param group  The attribute group.
     * @return The copied entity, which will be detached from the entity manager.
     */
    @SuppressWarnings("unchecked")
    protected <T> T copy(T entity, AttributeGroup group) {
        JpaEntityManager jem = em.unwrap(JpaEntityManager.class);
        return (T)jem.copy(entity, group);
    }

    /**
     * Find an entity by primary key.
     *
     * @param <T>         The entity type.
     * @param entityClass The entity class.
     * @param primaryKey  The primary key value.
     * @return The entity, or null if not found.
     */
    protected <T> T find(Class<T> entityClass, Object primaryKey) {
        return em.find(entityClass, primaryKey);
    }
}
