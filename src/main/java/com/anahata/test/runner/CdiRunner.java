package com.anahata.test.runner;

import org.jboss.weld.environment.se.Weld;
import org.jboss.weld.environment.se.WeldContainer;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.junit.runners.model.InitializationError;

/**
 * Execute unit tests with a custom JUnit 4 runner, which instantiates the tests with CDI so injection is available.
 * 
 * @author Robert Nagajek
 */
public class CdiRunner extends BlockJUnit4ClassRunner {
    private final Class klass;
    
    private final Weld weld;
    
    private final WeldContainer container;
    
    /**
     * Constructor.
     * 
     * @param klass The unit test class that will be tested.
     * @throws InitializationError If there is an error initializing.
     */
    public CdiRunner(final Class<?> klass) throws InitializationError {
        super(klass);
        this.klass = klass;
        this.weld = new Weld();
        this.container = weld.initialize();
    }

    @Override
    @SuppressWarnings("unchecked")
    protected Object createTest() throws Exception {
        return container.instance().select(klass).get();
    }
}
