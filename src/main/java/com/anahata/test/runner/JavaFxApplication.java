package com.anahata.test.runner;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import javafx.application.Application;
import javafx.stage.Stage;

/**
 * JavaFX application for JUnit 4.
 *
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
public class JavaFxApplication extends Application {
    private static boolean started;

    static void startJavaFX() {
        if (started) {
            return;
        }

        started = true;
        final ExecutorService executor = Executors.newSingleThreadExecutor();

        executor.execute(new Runnable() {
            @Override
            public void run() {
                JavaFxApplication.launch();
            }
        });
        
        // Pause briefly to give FX a chance to start.
        
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    static void launch() {
        Application.launch();
    }
    
    @Override
    public void start(Stage stage) throws Exception {
    }
}
