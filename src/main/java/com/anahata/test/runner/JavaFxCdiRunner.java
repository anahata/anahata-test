package com.anahata.test.runner;

import java.util.concurrent.CountDownLatch;
import javafx.application.Platform;
import org.junit.runner.notification.RunNotifier;
import org.junit.runners.model.FrameworkMethod;
import org.junit.runners.model.InitializationError;

/**
 * Run JUnit tests in the JavaFX thread and with CDI enabled.
 *
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
public class JavaFxCdiRunner extends CdiRunner {
    public JavaFxCdiRunner(Class<?> klass) throws InitializationError {
        super(klass);
        JavaFxApplication.startJavaFX();
    }

    @Override
    protected void runChild(final FrameworkMethod method, final RunNotifier notifier) {
        final CountDownLatch latch = new CountDownLatch(1);

        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                JavaFxCdiRunner.super.runChild(method, notifier);
                latch.countDown();
            }
        });
        
        try {
            latch.await();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
