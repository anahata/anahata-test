package com.anahata.test.runner;

import java.util.concurrent.CountDownLatch;
import javafx.application.Platform;
import org.junit.runner.notification.RunNotifier;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.junit.runners.model.FrameworkMethod;
import org.junit.runners.model.InitializationError;

/**
 * Run JUnit tests in the JavaFX thread.
 *
 * @author Robert Nagajek <robert@anahata-it.com.au>
 */
public class JavaFxRunner extends BlockJUnit4ClassRunner {
    public JavaFxRunner(Class<?> klass) throws InitializationError {
        super(klass);
        JavaFxApplication.startJavaFX();
    }

    @Override
    protected void runChild(final FrameworkMethod method, final RunNotifier notifier) {
        final CountDownLatch latch = new CountDownLatch(1);

        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                JavaFxRunner.super.runChild(method, notifier);
                latch.countDown();
            }
        });
        
        try {
            latch.await();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
